
import cv2
import glob
import random
import numpy as np


class update:

    # emotions = ["neutral", "anger", "disgust", "fear", "happy", "sadness", "surprise"] #Emotion list
    fishface = cv2.face.FisherFaceRecognizer_create() #Initialize fisher face classifier
    emotions = []
    data = {}

    def get_files(self, emotion): #Define function to get file list, randomly shuffle it and split 80/20
        files = glob.glob("dataset/%s/*" %emotion)
        random.shuffle(files)
        training = files[:int(len(files)*0.8)] #get first 80% of file list
        prediction = files[-int(len(files)*0.2):] #get last 20% of file list
        return training, prediction

    def make_sets(self):
        training_data = []
        training_labels = []
        prediction_data = []
        prediction_labels = []
        for emotion in self.emotions:
            training, prediction = self.get_files(emotion)
            #Append data to training and prediction list, and generate labels 0-7
            for item in training:
                image = cv2.imread(item) #open image
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) #convert to grayscale
                training_data.append(gray) #append image array to training data list
                training_labels.append(self.emotions.index(emotion))
        
            for item in prediction: #repeat above process for prediction set
                image = cv2.imread(item)
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                prediction_data.append(gray)
                prediction_labels.append(self.emotions.index(emotion))

        return training_data, training_labels, prediction_data, prediction_labels

    def run_recognizer(self):
        training_data, training_labels, prediction_data, prediction_labels = self.make_sets()
        
        print("training fisher face classifier")
        print("size of training set is:", len(training_labels), "images")
        self.fishface.train(training_data, np.asarray(training_labels))
        
        print("predicting classification set")
        cnt = 0
        correct = 0
        incorrect = 0
        for image in prediction_data:
            pred, conf = self.fishface.predict(image)
            if pred == prediction_labels[cnt]:
                correct += 1
                cnt += 1
            else:
                incorrect += 1
                cnt += 1
        accuracy = ((100*correct)/(correct + incorrect))
        print("Accuracy: " + str(accuracy))

    def __init__(self, ems):
        try:
            self.fishface.read("trained_emoclassifier.xml")
        except:
            print("Training Initial Emotions")
            self.emotions = ["anger", "disgust", "fear", "happy", "neutral", "sad", "surprise"]
            self.run_recognizer()
        
        print("Updating Model")
        self.emotions = ems
        self.run_recognizer()
        self.fishface.write("trained_emoclassifier.xml")
    
    def __del__(self):
        self.fishface.write("trained_emoclassifier.xml")
    # #Now run it
    # metascore = []
    # for i in range(0,10):
    #     correct = run_recognizer()
    #     print("got", correct, "percent correct!")
    #     metascore.append(correct)

    # print("\n\nend score:", np.mean(metascore), "percent correct!")