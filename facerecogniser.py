import cv2, numpy as np, argparse, time, glob, os, sys, subprocess, pandas, random, Update_Model, math

#load classifier
facecascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
fishface = cv2.face.FisherFaceRecognizer_create()
frame = None
try:
    print("Checking if model is trained...")
    fishface.read("trained_emoclassifier.xml")
    print("Models are trained. We're good to go...")
except Exception as e:
    print("Error: You do not have a trained model, please run program with --update flag first")
    exit()

# Initialize Camera
camnumber = 0
video_capture = cv2.VideoCapture(camnumber)

# Parse Arguments
parser = argparse.ArgumentParser(description="Options for the emotion-based music player")
parser.add_argument("--update", help="Call to grab new images and update the model accordingly", action="store_true")
args = parser.parse_args()

# Initialize Variables
facedict = {}
actions = {}

# Define Emotions
emotions = ["angry", "happy", "sad", "neutral"]

# Read Emotion-Song Mapping from Excel
df = pandas.read_excel("EmotionLinks.xlsx") #open Excel file
actions["angry"] = [x for x in df.angry.dropna()] #We need de dropna() when columns are uneven in length, which creates NaN values at missing places. The OS won't know what to do with these if we try to open them.
actions["happy"] = [x for x in df.happy.dropna()]
actions["sad"] = [x for x in df.sad.dropna()]
actions["neutral"] = [x for x in df.neutral.dropna()]

def open_stuff(filename): 
    """Open the file using native system player"""
    print("Playing: "+filename)
    
    filename = "music/%s" % (filename)
    
    if sys.platform == "win32":
        # For Windows
        os.startfile(filename)
    else:
        # For UNIX
        opener ="open" if sys.platform == "darwin" else "xdg-open"
        subprocess.call([opener, filename])

def crop_face(clahe_image, face):
    """Clear out face from frame"""
    for (x, y, w, h) in face:
        faceslice = clahe_image[y:y+h, x:x+w]
        faceslice = cv2.resize(faceslice, (350, 350))
    facedict["face%s" %(len(facedict)+1)] = faceslice
    return faceslice

def update_model(emotions):
    """Updates Model and trains it with new data"""

    print("Model update mode active")
    
    # Check for existence of dataset folders
    check_folders(emotions)
    
    # Get Training Data for each emotion
    for i in range(0, len(emotions)):
        save_face(emotions[i])

    print("collected images, looking good! Now updating model...")
    Update_Model.update(emotions)
    print("Done!")

def check_folders(emotions):
    """Checks for dataset folders for training"""
    
    for x in emotions:
        if os.path.exists("dataset/%s" %x):
            pass
        else:
            os.makedirs("dataset/%s" %x)

def save_face(emotion):
    """Capture and Save face data for emotion"""

    print("\n\nPlease look '" + emotion + "'. Press ENTER when you're ready to record this emotion.")
    
    raw_input() #Wait until enter is pressed with the raw_input() method

    # Open Camera
    video_capture.open(camnumber)

    # Capture 16 Frames
    while len(facedict.keys()) < 16:
        detect_face()
    
    # Relase Camera
    video_capture.release()

    # Store faces in dataset directory
    for x in facedict.keys():
        cv2.imwrite("dataset/%s/%s.jpg" %(emotion, len(glob.glob("dataset/%s/*" %emotion))), facedict[x])
    
    # Empty the dict
    facedict.clear() 
    
def recognize_emotion():
    """Recognizes the emotion, selects a random file on the emotion and plays it"""

    predictions = []
    confidence = []
    
    for x in facedict.keys():
        pred, conf = fishface.predict(facedict[x])
        cv2.imwrite("images/%s.jpg" %x, facedict[x])
        predictions.append(pred)
        confidence.append(conf)
    
    recognized_emotion = emotions[max(set(predictions), key=predictions.count)]
    
    print("I think you're %s" %recognized_emotion)
    
    # get list of files for detected emotion
    actionlist = [x for x in actions[recognized_emotion]] 
    # Randomly shuffle the list
    random.shuffle(actionlist) 
    # Open the first entry in the list
    open_stuff(actionlist[0]) 

def grab_webcamframe():
    """Captures and returns a single frame from webcam"""
    ret, frame = video_capture.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    clahe_image = clahe.apply(gray)
    return clahe_image

def detect_face():
    """Detects and returns a face from a frame"""
    clahe_image = grab_webcamframe()
    face = facecascade.detectMultiScale(clahe_image, scaleFactor=1.1, minNeighbors=15, minSize=(10, 10), flags=cv2.CASCADE_SCALE_IMAGE)

    if len(face) == 1: 
        faceslice = crop_face(clahe_image, face)
        # cv2.imshow("detect", faceslice) 
        return faceslice
    else:
        if len(face) == 0:
            print("\r Error: No Face Detected!")
        else:
            print("\r Error: Multiple Faces Detected!")

def run_detection():
    """Starts the detection"""
    print("Emotion Based Music Player!")
    print("\n\n Press Enter to start detection...")
    raw_input()

    print("Detecting Face")
    while len(facedict) != 10:
        detect_face()
    print("Recognizing Emotion")
    recognize_emotion()

if args.update:
    update_model(emotions)
else:
    video_capture.open(camnumber)
    run_detection()