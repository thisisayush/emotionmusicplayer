# Emotion Based Music Player

Based on Guide by Paul Vangent.
License: GNU 3.0 Open Source License.

## How to Setup?

Create a Python2 Virtual Environment and Install Requirements.

```
virtualenv -p python2 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Train the model

Though the model is pre-trained, it is recommended that you re-train the model with new data.
You can place your training files in dataset folder inside respective emotion and then start the training process.
P.S. You need to train with your face inititally.

```
python facerecogniser.py --update
```

## Create the song mapping

Store all your music files in music folder and add the names of files in `EmotionLinks.xlsx` for respective emotion. A
file will be randomly picked and played based on detected emotion.

## Start the script

```
python facerecogniser.py
```

Have Fun :)

## Citation(s):

van Gent, P. (2016). Emotion Recognition With Python, OpenCV and a Face Dataset. A tech blog about fun things with Python and embedded electronics. Retrieved from: http://www.paulvangent.com/2016/06/30/making-an-emotion-aware-music-player/